package com.epam.stakhiv.controller;

import com.epam.stakhiv.model.Section;
import com.epam.stakhiv.model.Seller;
import com.epam.stakhiv.view.View;

import java.util.Scanner;

public class Controller {
    Seller seller = new Seller();
    View view = new View();

    public void run() {
        menu();
    }

    Section section = new Section();

    public void menu() {
        seller.getProductList();
        view.printMenu1(seller.getWelcome(), seller.getOfferProducts());
        Scanner scanner = new Scanner(System.in);
        int select = scanner.nextInt();
        switch (select) {
            case 1:
                seller.getProductList();
                view.printMenu2(seller.getProductList());
                Scanner scanner2 = new Scanner(System.in);
                int select2 = scanner2.nextInt();
                switch (select2) {

                    case 1:
                        view.printAllProducts(section.getWoodProducts());
                        break;
                    case 2:
                        view.printAllProductsFromCheapToExpensive(section.getAllListFromCheapToExpensiveOfThisCategoryWood());
                        break;
                    case 3:
                        view.printAllProductsFromExpensiveToCheap(section.getAllListFromExpensiveToCheapOfThisCategoryWood());
                        break;
                    case 4:
                        System.out.println("Введіть максимальну суму: ");
                        Scanner scanner4 = new Scanner(System.in);
                        int maxSale = scanner4.nextInt();
                        view.printProductsFixedMoney(section.getAllListFixedSaleFor(maxSale));
                        break;
                    default:
                        System.out.println("Sorry, but I can't help you!");
                }
                break;

            case 2:
                seller.getProductList();
                view.printMenu2(seller.getProductList());
                Scanner scanner3 = new Scanner(System.in);
                int select3 = scanner3.nextInt();
                switch (select3) {
                    case 1:
                        view.printAllProducts(section.getPlumbingProducts());
                        break;
                    case 2:
                        view.printAllProductsFromCheapToExpensive(section.getAllListFromCheapToExpensiveOfThisCategoryPlumbing());
                        break;
                    case 3:
                        view.printAllProductsFromExpensiveToCheap(section.getAllListFromExpensiveToCheapOfThisCategoryPlumbing());
                        break;
                    case 4:
                        System.out.println("Введіть максимальну суму: ");
                        Scanner scanner5 = new Scanner(System.in);
                        int maxSale = scanner5.nextInt();
                        view.printProductsFixedMoney(section.getAllListFixedSalePlumbing(maxSale));
                        break;
                    default:
                        System.out.println("Sorry, but I can't help you!");
                }
                break;

            case 3:
                seller.getProductList();
                view.printMenu2(seller.getProductList());
                Scanner scanner4 = new Scanner(System.in);
                int select4 = scanner4.nextInt();
                switch (select4) {
                    case 1:
                        view.printAllProducts(section.getEverythingForTheyard());
                        break;
                    case 2:
                        view.printAllProductsFromCheapToExpensive(section.getAllListFromCheapToExpensiveOfThisCategoryFor());
                        break;
                    case 3:
                        view.printAllProductsFromExpensiveToCheap(section.getAllListFromExpensiveToCheapOfThisCategoryFor());
                        break;
                    case 4:
                        System.out.println("Введіть максимальну суму: ");
                        Scanner scanner6 = new Scanner(System.in);
                        int maxSale = scanner6.nextInt();
                        view.printProductsFixedMoney(section.getAllListFixedSaleFor(maxSale));
                        break;
                    default:
                        System.out.println("Sorry, but I can't help you!");
                }
                break;

            case 4:
                seller.getProductList();
                view.printMenu2(seller.getProductList());
                Scanner scanner5 = new Scanner(System.in);
                int select5 = scanner.nextInt();
                switch (select5) {
                    case 1:
                        view.printAllProducts(section.allProducts());
                        break;
                    case 2:
                        view.printAllProducts(section.allProductsFromCheapToExpensive());
                        break;
                    case 3:
                        view.printAllProducts(section.allProductsFromExpensiveToCheap());
                        break;
                    case 4:
                        System.out.println("Введіть максимальну суму: ");
                        Scanner scanner7 = new Scanner(System.in);
                        int maxSale = scanner7.nextInt();
                        view.printProductsFixedMoney(section.getAllFixedSale(maxSale));
                        break;
                    default:
                        System.out.println("Sorry, but I can't help you!");
                }
                break;
            default:
                System.out.println("Sorry, but I can't help you!");
                break;
        }
    }
}
