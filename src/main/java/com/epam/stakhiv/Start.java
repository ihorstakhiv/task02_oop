package com.epam.stakhiv;

import com.epam.stakhiv.controller.Controller;

public class Start {
    public static void main(String[] args) {
        Controller controller = new Controller();
        controller.run();
    }
}
