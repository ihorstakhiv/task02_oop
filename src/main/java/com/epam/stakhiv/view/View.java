package com.epam.stakhiv.view;

import com.epam.stakhiv.model.Product;
import java.util.ArrayList;
import java.util.List;

public class View {

    public void printMenu1(String welcome, String offerProducts) {
        System.out.println(welcome + offerProducts);
    }

    public void printMenu2(String productsList) {
        System.out.println(productsList);
    }

    public void printAllProducts(ArrayList<Product> products) {
        for (Product product : products) {
            System.out.println(product);
        }
    }

    public void printAllProductsFromCheapToExpensive(List<Product> products) {
        for (Product product : products) {
            System.out.println(product);
        }
    }

    public void printAllProductsFromExpensiveToCheap(List<Product> products) {
        for (Product product : products) {
            System.out.println(product);
        }
    }

    public void printProductsFixedMoney(List<Product> products) {
        for (Product product : products) {
            System.out.println(product);
        }
    }

    public void printAllProducts(List<Product> products) {
        for (Product product : products) {
            System.out.println(product);
        }
    }
}
