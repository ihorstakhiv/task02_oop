package com.epam.stakhiv.model;

import java.util.Objects;

public class Product {
    private String nameProduct;
    private String country;
    private int saleProduct;

    public Product(String nameProduct, String country, int saleProduct) {
        this.nameProduct = nameProduct;
        this.country = country;
        this.saleProduct = saleProduct;
    }

    public Product() {
    }

    public String getNameProduct() {
        return nameProduct;
    }

    public void setNameProduct(String nameProduct) {
        this.nameProduct = nameProduct;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public int getSaleProduct() {
        return saleProduct;
    }

    public void setSaleProduct(int saleProduct) {
        this.saleProduct = saleProduct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return saleProduct == product.saleProduct &&
                Objects.equals(nameProduct, product.nameProduct) &&
                Objects.equals(country, product.country);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nameProduct, country, saleProduct);
    }

    @Override
    public String toString() {
        return "Product{" +
                "nameProduct='" + nameProduct + '\'' +
                ", country='" + country + '\'' +
                ", saleProduct=" + saleProduct +
                '}';
    }
}
