package com.epam.stakhiv.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class Section {
    String nameOfSection;
    ArrayList<Product> products = new ArrayList<>();
    Market auchan = new Market();

    public ArrayList<Product> getWoodProducts() {
        Product tableUs = new Product("Table", "USA", 3400);
        Product tableUk = new Product("Table", "UKR", 1200);
        Product bedUs = new Product("Bed", "USA", 3900);
        Product bedFr = new Product("Bed", "France", 3200);
        Product chairUs = new Product("Chair", "USA", 870);
        products.add(tableUk);
        products.add(tableUs);
        products.add(bedFr);
        products.add(bedUs);
        products.add(chairUs);
        return products;
    }

    public ArrayList<Product> getPlumbingProducts() {
        Product showerUs = new Product("Shower", "USA", 456);
        Product showerUk = new Product("Shower", "UKR", 234);
        Product showerFr = new Product("Shower", "France", 200);
        Product bathFr = new Product("Bath", "France", 3540);
        Product bathUs = new Product("Bath", "USA", 8754);
        products.add(showerFr);
        products.add(showerUk);
        products.add(showerUs);
        products.add(bathFr);
        products.add(bathUs);
        return products;
    }

    public ArrayList<Product> getEverythingForTheyard() {
        Product treeUs = new Product("Apple tree", "USA", 100);
        Product treeUk = new Product("Apple tree", "UKR", 70);
        Product shovelFr = new Product("Shovel", "France", 220);
        Product shovelUk = new Product("Shovel", "UKR", 120);
        Product rakeUs = new Product("Rake", "USA", 450);
        products.add(treeUk);
        products.add(treeUs);
        products.add(shovelFr);
        products.add(shovelUk);
        products.add(rakeUs);
        return products;
    }


    public List<Product> getAllListFromCheapToExpensiveOfThisCategoryWood() {
        ArrayList<Product> woodProducts = getWoodProducts();
        List<Product> products = woodProducts.stream().sorted((o1, o2) -> o1.getSaleProduct() - o2.getSaleProduct()).collect(Collectors.toList());
        return products;
    }

    public List<Product> getAllListFromExpensiveToCheapOfThisCategoryWood() {
        ArrayList<Product> woodProducts = getWoodProducts();
        List<Product> products = woodProducts.stream().sorted((o1, o2) -> o2.getSaleProduct() - o1.getSaleProduct()).collect(Collectors.toList());
        return products;
    }

    public List<Product> getAllListFromCheapToExpensiveOfThisCategoryPlumbing() {
        ArrayList<Product> plumbingProducts = getPlumbingProducts();
        List<Product> products = plumbingProducts.stream().sorted((o1, o2) -> o1.getSaleProduct() - o2.getSaleProduct()).collect(Collectors.toList());
        return products;
    }

    public List<Product> getAllListFromExpensiveToCheapOfThisCategoryPlumbing() {
        ArrayList<Product> plumbingProducts = getPlumbingProducts();
        List<Product> products = plumbingProducts.stream().sorted((o1, o2) -> o2.getSaleProduct() - o1.getSaleProduct()).collect(Collectors.toList());
        return products;
    }

    public List<Product> getAllListFromCheapToExpensiveOfThisCategoryFor() {
        ArrayList<Product> forYardProducts = getEverythingForTheyard();
        List<Product> products = forYardProducts.stream().sorted((o1, o2) -> o1.getSaleProduct() - o2.getSaleProduct()).collect(Collectors.toList());
        return products;
    }

    public List<Product> getAllListFromExpensiveToCheapOfThisCategoryFor() {
        ArrayList<Product> forYardProducts = getEverythingForTheyard();
        List<Product> products = forYardProducts.stream().sorted((o1, o2) -> o2.getSaleProduct() - o1.getSaleProduct()).collect(Collectors.toList());
        return products;
    }

    public List<Product> getAllListFixedSaleFor(int sale) {
        List<Product> products = getAllListFromCheapToExpensiveOfThisCategoryFor().stream().filter(product -> product.getSaleProduct() <= sale).collect(Collectors.toList());
        return products;
    }

    public List<Product> getAllListFixedSalePlumbing(int sale) {
        List<Product> products = getAllListFromCheapToExpensiveOfThisCategoryPlumbing().stream().filter(product -> product.getSaleProduct() <= sale).collect(Collectors.toList());
        return products;
    }

    public List<Product> getAllListFixedSaleWood(int sale) {
        List<Product> products = getAllListFromCheapToExpensiveOfThisCategoryWood().stream().filter(product -> product.getSaleProduct() <= sale).collect(Collectors.toList());
        return products;
    }

    public List<Product> allProducts() {
        ArrayList<Product> woodProducts = getWoodProducts();
        ArrayList<Product> plumbingProducts = getPlumbingProducts();
        ArrayList<Product> everythingForTheyard = getEverythingForTheyard();
        HashSet<Product> allProducts = new HashSet<>();
        allProducts.addAll(woodProducts);
        allProducts.addAll(plumbingProducts);
        allProducts.addAll(everythingForTheyard);
        List<Product> allProductss = allProducts.stream().collect(Collectors.toList());
        return allProductss;
    }

    public List<Product> allProductsFromCheapToExpensive() {
        List<Product> products = allProducts();
        List<Product> products1 = products.stream().sorted((o1, o2) -> o1.getSaleProduct() - o2.getSaleProduct()).collect(Collectors.toList());
        return products1;
    }

    public List<Product> allProductsFromExpensiveToCheap() {
        List<Product> products = allProducts();
        List<Product> products1 = products.stream().sorted((o1, o2) -> o2.getSaleProduct() - o1.getSaleProduct()).collect(Collectors.toList());
        return products1;
    }

    public List<Product> getAllFixedSale(int sale) {
        List<Product> products = allProducts();
        List<Product> products1 = products.stream().filter(product -> product.getSaleProduct() <= sale).collect(Collectors.toList());
        return products1;
    }

    public Section(String nameOfSection, ArrayList<Product> products) {
        this.nameOfSection = nameOfSection;
        this.products = products;
    }

    public Section() {
    }

    public String getNameOfSection() {
        return nameOfSection;
    }

    public void setNameOfSection(String nameOfSection) {
        this.nameOfSection = nameOfSection;
    }

    public ArrayList<Product> getProducts() {
        return products;
    }

    public void setProducts(ArrayList<Product> products) {
        this.products = products;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Section section = (Section) o;
        return Objects.equals(nameOfSection, section.nameOfSection) &&
                Objects.equals(products, section.products);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nameOfSection, products);
    }

    @Override
    public String toString() {
        return "Section{" +
                "nameOfSection='" + nameOfSection + '\'' +
                ", products=" + products +
                '}';
    }
}
