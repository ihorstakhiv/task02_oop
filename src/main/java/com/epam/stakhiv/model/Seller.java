package com.epam.stakhiv.model;

public class Seller  {

    private String welcome ="Hello! Do you want to buy something ?";
    private String offerProducts ="We have 3 section of products: "+"\n"+
            "1) Wood products "+"\n"+
            "2) Plumbing "+"\n"+
            "3) Everything for the yard"+"\n"+
            "4) Show you all list of products"+"\n"+
            "What would be interesting for you?"+"\n";
    private String productList =
            "1)I can show you all list of this category"+"\n"+
            "2)I can show you all list from cheap to expensive of this category"+"\n"+
            "3)I can show you all list from expensive to cheap of this category"+"\n"+
            "4)Or if you can a fixed money you can say this sum and I show you this list";

    public String getWelcome() {
        return welcome;
    }

    public String getOfferProducts() {
        return offerProducts;
    }

    public String getProductList() {
        return productList;
    }

    public Seller(String welcome, String offerProducts, String productList) {
        this.welcome = welcome;
        this.offerProducts = offerProducts;
        this.productList = productList;
    }

    @Override
    public String toString() {
        return "Seller{" +
                "welcome='" + welcome + '\'' +
                ", offerProducts='" + offerProducts + '\'' +
                ", productList='" + productList + '\'' +
                '}';
    }

    public Seller() {
    }
}
