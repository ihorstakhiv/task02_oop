package com.epam.stakhiv.model;

import java.util.Objects;

public class Market {
    Section section;

    public Market(Section section) {
        this.section = section;
    }

    public Market() {
    }

    public Section getSection() {
        return section;
    }

    public void setSection(Section section) {
        this.section = section;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Market market = (Market) o;
        return Objects.equals(section, market.section);
    }

    @Override
    public int hashCode() {
        return Objects.hash(section);
    }

    @Override
    public String toString() {
        return "Market{" +
                "section=" + section +
                '}';
    }
}
